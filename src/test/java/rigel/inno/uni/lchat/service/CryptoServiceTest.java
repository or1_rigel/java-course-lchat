package rigel.inno.uni.lchat.service;

import com.sun.xml.internal.rngom.parse.host.Base;
import org.junit.Test;

import java.util.Base64;

import static org.junit.Assert.*;

/**
 * r1gel on 28/12/2016.
 */
public class CryptoServiceTest {
    @Test
    public void salt() throws Exception {
        CryptoService cryptoService = new CryptoService();
        byte[] salt = cryptoService.salt();
        assertEquals(salt.length, 32);
    }

    @Test
    public void hashPassword() throws Exception {
        CryptoService cryptoService = new CryptoService();
        byte[] salt = cryptoService.salt();
        byte[] hash = cryptoService.hashPassword(salt, "password".toCharArray());
        String hastStr = Base64.getEncoder().encodeToString(hash);
        assertNotNull(hastStr);
    }

}