<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="static/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="static/css/main.css">
</head>
<body>
    <div class="container center-div">
        <form action="login" method="post">
            <div class="form-group" >
                <label for="email" >Email:</label>
                <input type="text" class="form-control" name="email" placeholder="Email" id="email">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" type="password" name="password" placeholder="Password" id="password">
            </div>
            <button type="submit" class="btn btn-primary">Login</button>
            <a href="signup" class="btn btn-default">Sign Up</a>
        </form>
    </div>
</body>
</html>
