<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Users</title>
    <link rel="stylesheet" type="text/css" href="static/css/bootstrap.css">
  </head>
  <body>
  <nav class="navbar navbar-inverse">
      <div class="container-fluid">
          <div class="navbar-header">
              <a class="navbar-brand" href="#">LChat</a>
          </div>
          <ul class="nav navbar-nav">
              <li><a href="main">Users</a></li>
              <li><a href="profile">Profile</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
                 <li><p class="navbar-text">Hello, ${name}</p></li>
                 <li><a href="logout"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
          </ul>
      </div>
  </nav>

  <table class="table">
      <thead>
        <tr>
          <th>UUID</th>
          <th>Email</th>
          <th>Name</th>
          <th></th>
        </tr>
      </thead>
      <tbody>

      <c:forEach items="${users}" var="user">
          <tr>
              <th class="col-md-3" scope="row"><c:out value="${user.uuid}" /></th>
              <td class="col-md-3"><c:out value="${user.email}"/></td>
              <td class="col-md-3"><c:out value="${user.name}"/></td>
              <td class="col-md-3"><button class="btn btn-default">Chat</button></td>
          </tr>
      </c:forEach>
      </tbody>
  </table>

  </body>
</html>
