package rigel.inno.uni.lchat.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rigel.inno.uni.lchat.dao.DaoFactory;
import rigel.inno.uni.lchat.dao.model.User;
import rigel.inno.uni.lchat.service.UserService;
import rigel.inno.uni.lchat.service.model.UserEntity;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class MainServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(MainServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        UserEntity user = (UserEntity) session.getAttribute("user");
        if(user != null) {
            req.setAttribute("auth", true);
            req.setAttribute("name", user.getName());
            req.setAttribute("role", "user");
        }

        UserService us = UserService.getInstance();

        List<UserEntity> users = us.find();
        req.setAttribute("users", users);

        RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/pages/main.jsp");
        rd.forward(req, resp);
    }
}
