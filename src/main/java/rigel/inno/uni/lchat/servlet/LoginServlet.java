package rigel.inno.uni.lchat.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rigel.inno.uni.lchat.dao.DaoFactory;
import rigel.inno.uni.lchat.dao.model.User;
import rigel.inno.uni.lchat.service.UserService;
import rigel.inno.uni.lchat.service.model.UserEntity;

import javax.jws.soap.SOAPBinding;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class LoginServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(LoginServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/pages/login.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        UserService us = UserService.getInstance();
        UserEntity user = us.findByEmail(email);

        if(user != null && us.comparePassword(user, password)) {
            HttpSession session = req.getSession(true);
            session.setAttribute("user", user);
        }

        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/main"));
    }
}
