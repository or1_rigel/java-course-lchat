package rigel.inno.uni.lchat.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rigel.inno.uni.lchat.dao.model.User;
import rigel.inno.uni.lchat.service.model.UserEntity;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class MainFilter implements Filter {
    private static final Logger logger = LoggerFactory.getLogger(MainFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse)  response;

        HttpSession session = req.getSession();
        UserEntity user = (UserEntity) session.getAttribute("user");

        if(user == null) {
            res.sendRedirect(res.encodeRedirectURL(req.getContextPath() + "/login"));
            return;
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
