package rigel.inno.uni.lchat.chat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.*;


@ServerEndpoint(value = "/chat/{room}")
public class WSEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(WSEndpoint.class);

    private Map<String, ChatRoom> rooms = new HashMap<>();
    private List<Session> sessions = new ArrayList<>();

    @OnOpen
    public void onOpen(final Session session, @PathParam("room") final String roomUUID) {
        sessions.add(session);
        if(rooms.containsKey(roomUUID)) {
            ChatRoom room = rooms.get(roomUUID);

        }
    }

    @OnClose
    public void onClose(final Session session) {
        sessions.remove(session);
    }

    @OnMessage
    public void onMessage(final String message, final Session client) {
        try {
            logger.info("Message: {}", message);
            for(final Session session : sessions) {
                session.getBasicRemote().sendObject(message);
            }
        } catch (IOException e) {
            logger.error("IO error", e);
        } catch (EncodeException e) {
            logger.error("Encoding error", e);
        }
    }
}
