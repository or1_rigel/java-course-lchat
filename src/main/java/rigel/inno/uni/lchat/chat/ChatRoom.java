package rigel.inno.uni.lchat.chat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.Session;
import java.util.ArrayList;
import java.util.List;

public class ChatRoom {
    private static final Logger logger = LoggerFactory.getLogger(ChatRoom.class);

    List<String> users;

    public ChatRoom() {
        users = new ArrayList<>();
    }

    public void add(String userUUID) {
       users.add(userUUID);
    }

    public boolean hasUser(String userUuid) {
        for(String uuid : users) {
            if(uuid.equals(userUuid)) {
                return true;
            }
        }

        return false;
    }
}
