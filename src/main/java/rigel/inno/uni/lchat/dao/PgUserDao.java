package rigel.inno.uni.lchat.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rigel.inno.uni.lchat.dao.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class PgUserDao implements UserDao<User, UUID>{
    private static final Logger logger = LoggerFactory.getLogger(PgUserDao.class);

    private Connection connection;


    public PgUserDao(Connection connection) {
        this.connection = connection;
    }


    // TODO: выбрасывать свои Exceptions
    private final String FIND = "SELECT * FROM public.user";
    private final String FIND_BY_EMAIL = "SELECT * FROM public.user WHERE email = ?";
    private final String FIND_BY_ID = "SELECT * FROM public.user WHERE id = ?";

    private final String ADD = "INSERT INTO public.user (\"email\", \"name\", \"salt\", \"passwordHash\") VALUES (?, ?, ?, ?)";
    private final String UPDATE = "UPDATE public.user SET \"email\"=?, \"name\"=? WHERE uuid = ?";
    private final String DELETE = "DELETE FROM public.user WHERE uuid = ?";



    @Override
    public List<User> find() {

        try {
            List<User> users = new ArrayList<>();

            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(FIND);

            while(set.next()) {
               users.add(map(set));
            }

            return users;
        } catch (SQLException e) {
            logger.error("SQL error", e);
            return null;
        }
    }

    @Override
    public User findById(UUID uuid) {
        try {
            PreparedStatement statement = connection.prepareStatement(FIND_BY_ID);
            statement.setString(1, uuid.toString());
            ResultSet set = statement.executeQuery();

            User user = null;

            while(set.next()) {
                user = map(set);
            }

            return user;
        } catch (SQLException e) {
            logger.error("SQL error", e);
            return null;
        }
    }

    @Override
    public User add(User model) {
        try {
            PreparedStatement statement = connection.prepareStatement(ADD, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, model.getEmail());
            statement.setString(2, model.getName());
            statement.setString(3, model.getSalt());
            statement.setString(4, model.getPasswordHash());

            statement.executeUpdate();

            ResultSet set = statement.getGeneratedKeys();
            UUID resultUuid = null;
            while(set.next()) {
                resultUuid = UUID.fromString(set.getString(1));
            }

            return new User(resultUuid, model.getEmail(), model.getName(), model.getSalt(), model.getPasswordHash());

        } catch (SQLException e) {
            logger.error("SQL error", e);
            return null;
        }
    }

    @Override
    public void update(User model) {
        try {

            PreparedStatement statement = connection.prepareStatement(UPDATE);
            statement.setString(1, model.getEmail());
            statement.setString(2, model.getName());
            statement.setString(3, model.getUuid().toString());
            statement.executeUpdate();

        } catch (SQLException e) {
            logger.error("SQL error", e);
            return;
        }
    }

    @Override
    public void delete(User model) {
        try {
            PreparedStatement statement = connection.prepareStatement(DELETE);
            statement.setString(1, model.getUuid().toString());
        } catch (SQLException e) {
            logger.error("SQL error", e);
            return;
        }
    }

    @Override
    public User findByEmail(String email) {
        try {
            PreparedStatement statement = connection.prepareStatement(FIND_BY_EMAIL);
            statement.setString(1, email);
            ResultSet set = statement.executeQuery();

            User user = null;
            while (set.next()) {
                user = map(set);
            }

            return user;

        } catch (SQLException e) {
            logger.error("SQL error", e);
            return null;
        }
    }

    private User map(ResultSet set) {
        try {
            User user = new User();
            user.setUuid(UUID.fromString(set.getString("uuid")));
            user.setName(set.getString("name"));
            user.setEmail(set.getString("email"));
            user.setSalt(set.getString("salt"));
            user.setPasswordHash(set.getString("passwordHash"));
            return user;
        } catch (SQLException e) {
            logger.error("User mapping error", e);
            return null;
        }
    }
}
