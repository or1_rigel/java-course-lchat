package rigel.inno.uni.lchat.dao.model;

import java.util.UUID;

public class User {

    private UUID uuid;
    private String email;
    private String name;
    private String salt;
    private String passwordHash;

    public User() { }

    public User(UUID uuid, String email, String name,  String salt, String passwordHash) {
        this.uuid = uuid;
        this.email = email;
        this.name = name;
        this.passwordHash = passwordHash;
        this.salt = salt;
    }

    public User(String email, String name, String salt, String passwordHash) {
        this(null, email, name, salt, passwordHash);
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uid) {
        this.uuid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Override
    public String toString() {
        return "User{" +
                "uuid=" + uuid +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
