package rigel.inno.uni.lchat.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rigel.inno.uni.lchat.dao.model.User;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.ConnectionPoolDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;


public class DaoFactory {
    private static final Logger logger = LoggerFactory.getLogger(DaoFactory.class);

    private static ConnectionPoolDataSource connectionPoolDataSource;

    static {
        try {
            InitialContext context = new InitialContext();
            connectionPoolDataSource = (ConnectionPoolDataSource) context.lookup("java:comp/env/jdbc/lchat");
        } catch (NamingException e) {
            logger.error("Naming error", e);
        }
    }

    public static UserDao<User, UUID> getUserDao() {
        try {
            Connection connection = connectionPoolDataSource.getPooledConnection().getConnection();
            return new PgUserDao(connection);
        } catch (SQLException e) {
            logger.error("Get connection from pool error", e);
            return null;
        }
    }
}
