package rigel.inno.uni.lchat.dao;

import java.io.Serializable;
import java.util.List;

public interface Dao<T, K extends Serializable> {
    List<T> find();
    T findById(K id);

    T add(T model);
    void update(T model);
    void delete(T model);
}
