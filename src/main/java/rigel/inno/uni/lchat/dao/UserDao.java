package rigel.inno.uni.lchat.dao;

import java.io.Serializable;

public interface UserDao<T, K extends Serializable> extends Dao <T, K> {
    T findByEmail(String email);
}
