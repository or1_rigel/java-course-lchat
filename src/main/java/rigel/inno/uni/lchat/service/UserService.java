package rigel.inno.uni.lchat.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rigel.inno.uni.lchat.dao.DaoFactory;
import rigel.inno.uni.lchat.dao.UserDao;
import rigel.inno.uni.lchat.dao.model.User;
import rigel.inno.uni.lchat.service.model.UserEntity;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

import static rigel.inno.uni.lchat.service.ServiceToDaoMapper.mapToDao;
import static rigel.inno.uni.lchat.service.ServiceToDaoMapper.mapToService;

public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private UserDao<User, UUID> dao = DaoFactory.getUserDao();

    private UserService() { }

    private static class Lazy {
        private static final UserService instance = new UserService();
    }

    public static UserService getInstance() {
       return Lazy.instance;
    }


    public UserEntity singUpUser(String email, String name, String password) {
        CryptoService cs = new CryptoService();
        byte[] salt = cs.salt();
        String passwordHash = cs.hashPasswordBase64(salt, password.toCharArray());
        UserEntity user = new UserEntity(email, name, Base64.getEncoder().encodeToString(salt), passwordHash);
        return add(user);
    }

    public boolean comparePassword(UserEntity user, String password) {
        // TODO: придумать как упростить
        byte[] salt = Base64.getDecoder().decode(user.getSalt());
        CryptoService cs = new CryptoService();
        String comparable = cs.hashPasswordBase64(salt, password.toCharArray());

        return comparable.equals(user.getPasswordHash());

    }

    /**
     * Возвращает cписок пользователей
     * @return список пользователей
     */
    public List<UserEntity> find() {
        List<User> daoUsers = dao.find();
        List<UserEntity> users = new ArrayList<>(daoUsers.size());

        for(User daoUser : daoUsers) {
            users.add(mapToService(daoUser));
        }

        return users;
    }

    /**
     * Поиск пользователя по email
     * @param email
     * @return пользователь, либо null
     */
    public UserEntity findByEmail(String email) {
        return mapToService(dao.findByEmail(email));
    }

    /**
     * Поиск пользователя по uuid
     * @param uuid
     * @return
     */
    public UserEntity findByUuid(UUID uuid) {
        return mapToService(dao.findById(uuid));
    }

    /**
     * Добавляет пользователя в базу данных
     * @param user
     * @return
     */
    public UserEntity add(UserEntity user) {
        return mapToService(dao.add(mapToDao(user)));
    }


    public void update(UserEntity user) {
        dao.update(mapToDao(user));
    }

    public void delete(UserEntity user) {
        dao.delete(mapToDao(user));
    }
}
