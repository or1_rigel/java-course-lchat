package rigel.inno.uni.lchat.service.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class UserEntity {
    private static final Logger logger = LoggerFactory.getLogger(UserEntity.class);

    private UUID uuid;
    private String email;
    private String name;

    /**
     * Password hash in base64
     */
    private String passwordHash;

    /**
     * Random salt in base64
     */
    private String salt;

    public UserEntity() { }


    public UserEntity(UUID uuid, String email, String name, String salt, String passwordHash) {
        this.uuid = uuid;
        this.email = email;
        this.name = name;
        this.passwordHash = passwordHash;
        this.salt = salt;
    }

    public UserEntity(String email, String name, String salt, String passwordHash) {
        this(null, email, name, salt, passwordHash);
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
