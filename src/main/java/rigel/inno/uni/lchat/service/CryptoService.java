package rigel.inno.uni.lchat.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.interfaces.PBEKey;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Random;

/**
 * Генерация хешей для пароля
 */
public class CryptoService {
    private static final Logger logger = LoggerFactory.getLogger(CryptoService.class);

    public final static int SALT_LEN = 32;
    public CryptoService() { }

    /**
     * Генерирует "соль" для пароля
     * @return byte[] c солью
     */
    public byte[] salt() {
       final Random random = new SecureRandom();
       byte[] salt = new byte[SALT_LEN];
       random.nextBytes(salt);
       return salt;
    }

    /**
     * Хеш из пароля и соли
     * @param salt
     * @param password
     * @return byte[] hash
     */

    public byte[] hashPassword(final byte[] salt, final char[] password) {
        try {
            // TODO: медленно
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            PBEKeySpec spec = new PBEKeySpec(password, salt, 1, 256);
            SecretKey key = skf.generateSecret(spec);
            return key.getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            logger.error("Bad algorithm", e);
            throw new RuntimeException();
        }
    }

    public String hashPasswordBase64(final byte[] salt, final char[] password) {
        return Base64.getEncoder().encodeToString(hashPassword(salt, password));
    }
}
