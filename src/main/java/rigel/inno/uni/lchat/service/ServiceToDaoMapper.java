package rigel.inno.uni.lchat.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rigel.inno.uni.lchat.dao.model.User;
import rigel.inno.uni.lchat.service.model.UserEntity;

public class ServiceToDaoMapper {
    private static final Logger logger = LoggerFactory.getLogger(ServiceToDaoMapper.class);

    public static UserEntity mapToService(User user) {
        return new UserEntity(user.getUuid(), user.getEmail(), user.getName(), user.getSalt(), user.getPasswordHash());
    }


    public static User mapToDao(UserEntity user) {
        return new User(user.getUuid(), user.getEmail(), user.getName(), user.getSalt(), user.getPasswordHash());
    }
}
